package mk.ukim.finki.emt2019.ordermanagement.service.impl;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.service.ProductQuantityValidator;
import org.springframework.stereotype.Component;

/**
 * @author Riste Stojanov
 */
@Component
public class PositiveQuantityValidator implements ProductQuantityValidator {
    @Override
    public boolean isProductQuantityValid(Quantity quantity) {
        return quantity.quantity > 0D;
    }
}
