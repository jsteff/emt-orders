package mk.ukim.finki.emt2019.ordermanagement.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author Riste Stojanov
 */
@Entity
public class Product {

    @Id
    @NotNull
    public Long productId;

    public String displayName;

    public Double price;

    public Quantity stockQuantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return productId.equals(product.productId);
    }

    @Override
    public int hashCode() {
        return productId.hashCode();
    }
}
