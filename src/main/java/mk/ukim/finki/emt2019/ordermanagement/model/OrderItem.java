package mk.ukim.finki.emt2019.ordermanagement.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Riste Stojanov
 */
@Entity
@Table(name = "order_items")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long orderItemId;

    @ManyToOne
    public Product product;

    public Double price;

    public Quantity quantity;

    public LocalDateTime expirationTime;

    public static OrderItem createWithExpiryInHours(Long expiresAfterHours, Product product, Quantity quantity) {
        OrderItem item = new OrderItem();
        item.expirationTime = LocalDateTime.now().plusHours(expiresAfterHours);
        item.product = product;
        item.price = product.price;
        item.quantity = quantity;
        return item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderItem item = (OrderItem) o;

        return Objects.equals(orderItemId, item.orderItemId);
    }

    @Override
    public int hashCode() {
        return orderItemId != null ? orderItemId.hashCode() : 0;
    }
}
