package mk.ukim.finki.emt2019.ordermanagement.model;

/**
 * @author Riste Stojanov
 */
public enum Unit {
    PEACES,
    LITER,
    MB,
    GB,
    KG,
    VOLUME__M_3
}
