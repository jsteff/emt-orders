package mk.ukim.finki.emt2019.ordermanagement.repository;

import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ShoppingCartRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Riste Stojanov
 */
@Service
@Profile("test")
//@Profile("testmem")
public class TxHelperImpl implements TxHelper{

    private final ShoppingCartRepository cartRepository;

    public TxHelperImpl(ShoppingCartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Transactional(readOnly = true)
    public ShoppingCart readCartInTx(Long cartId) {
        ShoppingCart cart = this.cartRepository.findById(cartId).get();

        System.out.println(cart.items.size());
        return cart;
    }


    @Transactional(readOnly = true)
    public ShoppingCart fetchCartInTx(Long cartId) {
        ShoppingCart cart = this.cartRepository.fetchById(cartId).get();

        System.out.println(cart.items.size());
        return cart;
    }
}
